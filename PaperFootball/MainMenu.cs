﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PaperFootball
{
    public partial class MainMenu : Form
    {
        public MainMenu()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FieldForm field = new FieldForm();
            field.FormClosed += new FormClosedEventHandler((object o, FormClosedEventArgs f) => Show());
            field.Show();
            Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            RulesForm field = new RulesForm();
            field.FormClosed += new FormClosedEventHandler((object o, FormClosedEventArgs f) => Show());
            field.Show();
            Hide();
        }
    }
}
