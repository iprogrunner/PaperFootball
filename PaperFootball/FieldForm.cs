﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PaperFootball
{
    public partial class FieldForm : Form
    {
        private Rectangle[,] points;
        private List<Point> path;
        private List<Tuple<int, int>> pathIndex;
        private const int pointSize = 6;
        private const int fieldWidth = 21;
        private const int fieldHeight = 14;
        private const int goalSize = 6;
        private bool player1_turn;
        private bool penalty;
        private byte lines_left_in_turn;

        public FieldForm()
        {
            InitializeComponent();
            points = new Rectangle[fieldHeight, fieldWidth];
            path = new List<Point>();
            pathIndex = new List<Tuple<int, int>>();
            pathIndex.Add(new Tuple<int, int>(fieldHeight / 2, fieldWidth / 2));
            player1_turn = true;
            label_player1.BackColor = Color.Green;
            label_pl1_lines.Text = "3";
            lines_left_in_turn = 3;
            penalty = false;
            CalcPoints();
            path.Add(new Point(
                points[pathIndex.Last().Item1, pathIndex.Last().Item2].X + pointSize / 2,
                points[pathIndex.Last().Item1, pathIndex.Last().Item2].Y + pointSize / 2));
            Draw();
        }

        private void CalcPoints()
        {
            for (int i = 0; i < fieldHeight; i++)
                for (int j = 0; j < fieldWidth; j++) points[i, j] = new Rectangle(
                    new Point(j * (pbField.Width - 2 * pointSize) / (fieldWidth - 1) + pointSize / 2,
                        i * (pbField.Height - 2 * pointSize) / (fieldHeight - 1) + pointSize / 2),
                    new Size(pointSize, pointSize));
        }

        private void Draw()
        {
            DrawPoints();
            DrawLines();
            DrawPossibleSteps();
            pbField.Refresh();
        }

        private void DrawPoints()
        {
            pbField.Image = new Bitmap(pbField.Width, pbField.Height);
            Graphics graphics = Graphics.FromImage(pbField.Image);
            Pen pen = new Pen(Color.Blue, 3);
            graphics.Clear(DefaultBackColor);
            foreach (Rectangle p in points) graphics.DrawEllipse(pen, p);
            pen.Color = Color.Red;
            graphics.DrawEllipse(pen, points[pathIndex.Last().Item1, pathIndex.Last().Item2]);
        }

        private void DrawLines()
        {
            Graphics graphics = Graphics.FromImage(pbField.Image);
            Pen pen = new Pen(Color.Blue, 3);
            if (path.Count > 1) graphics.DrawLines(pen, path.ToArray()); 
        }

        private void pbField_MouseClick(object sender, MouseEventArgs e)
        {
            for (int i = 0; i < fieldHeight; i++)
                for (int j = 0; j < fieldWidth; j++)
                    if (points[i, j].Contains(e.Location))
                        if (checkRules(i, j))
                        {
                            AddPoint(new Tuple<int, int>(i, j));
                            if (penalty)
                            {
                                penalty = false;
                                lines_left_in_turn = 1;
                            }
                            Draw();
                            lines_left_in_turn--;
                            if (player1_turn) label_pl1_lines.Text = lines_left_in_turn.ToString();
                            else label_pl2_lines.Text = lines_left_in_turn.ToString();
                            if ((lines_left_in_turn == 0 && !penalty) || (lines_left_in_turn > 0 && penalty))
                                ChangeTurn();
                            else CheckWinCondition();
                            return;
                        }
        }

        private void CheckWinCondition()
        {
            //Проверить в случае пеналти
            Tuple<int, int> last = pathIndex.Last();
            if (penalty)
            {
                int dst;
                if (player1_turn)
                {
                    dst = fieldWidth - 1 - last.Item2;
                    if (dst < 6 && Math.Abs(2 * last.Item1 - fieldHeight + 1) < goalSize + dst)
                        button2.Enabled = true;
                }
                else
                {
                    dst = last.Item2;
                    if (dst < 6 && Math.Abs(2 * last.Item1 - fieldHeight + 1) < goalSize + dst)
                        button1.Enabled = true;
                }
            }
            else
            {
                if (player1_turn)
                {
                    if (last.Item2 == fieldWidth - 1 && Math.Abs(2 * last.Item1 - fieldHeight + 1) < goalSize)
                        button2.Enabled = true;
                }
                else if (last.Item2 == 0 && Math.Abs(2 * last.Item1 - fieldHeight + 1) < goalSize)
                    button1.Enabled = true;
            }
        }

        private void ChangeTurn()
        {
            player1_turn = !player1_turn;
            lines_left_in_turn = 3;
            if (player1_turn)
            {
                label_player1.BackColor = Color.Green;
                label_player2.BackColor = DefaultBackColor;
                label_pl1_lines.Text = "3";
            }
            else
            {
                label_player1.BackColor = DefaultBackColor;
                label_player2.BackColor = Color.Green;
                label_pl2_lines.Text = "3";
            }
        }

        private void AddPoint(Tuple<int,int> t)
        {
            if (penalty)
            {
                Tuple<int, int> next = pathIndex.Last();
                for (int i=0; i<6; i++)
                {
                    next = new Tuple<int, int>(
                        next.Item1 + Math.Sign(t.Item1 - next.Item1),
                        next.Item2 + Math.Sign(t.Item2 - next.Item2));
                    pathIndex.Add(next);
                    path.Add(new Point(
                                points[next.Item1, next.Item2].X + pointSize / 2,
                                points[next.Item1, next.Item2].Y + pointSize / 2));
                }
            }
            else
            {
                pathIndex.Add(t);
                path.Add(new Point(
                                points[t.Item1, t.Item2].X + pointSize / 2,
                                points[t.Item1, t.Item2].Y + pointSize / 2));
            }
        }

        private void DrawPossibleSteps()
        {
            //if penalty wrong green points
            Graphics graphics = Graphics.FromImage(pbField.Image);
            Pen pen = new Pen(Color.Green, 3);
            Tuple<int, int> last = pathIndex.Last();
            bool have_steps = false;
            int shift = penalty ? 6 : 1;
            if (checkRules(last.Item1, last.Item2 - shift))
            {
                graphics.DrawEllipse(pen, points[last.Item1, last.Item2 - shift]);
                have_steps = true;
            }
            if (checkRules(last.Item1, last.Item2 + shift))
            {
                graphics.DrawEllipse(pen, points[last.Item1, last.Item2 + shift]);
                have_steps = true;
            }
            if (checkRules(last.Item1 - shift, last.Item2 - shift))
            {
                graphics.DrawEllipse(pen, points[last.Item1 - shift, last.Item2 - shift]);
                have_steps = true;
            }
            if (checkRules(last.Item1 - shift, last.Item2))
            {
                graphics.DrawEllipse(pen, points[last.Item1 - shift, last.Item2]);
                have_steps = true;
            }
            if (checkRules(last.Item1 - shift, last.Item2 + shift))
            {
                graphics.DrawEllipse(pen, points[last.Item1 - shift, last.Item2 + shift]);
                have_steps = true;
            }
            if (checkRules(last.Item1 + shift, last.Item2 - shift))
            {
                graphics.DrawEllipse(pen, points[last.Item1 + shift, last.Item2 - shift]);
                have_steps = true;
            }
            if (checkRules(last.Item1 + shift, last.Item2))
            {
                graphics.DrawEllipse(pen, points[last.Item1 + shift, last.Item2]);
                have_steps = true;
            }
            if (checkRules(last.Item1 + shift, last.Item2 + shift))
            {
                graphics.DrawEllipse(pen, points[last.Item1 + shift, last.Item2 + shift]);
                have_steps = true;
            }
          
            if (!have_steps)
                if (!penalty)
                {
                    penalty = true;
                    DrawPossibleSteps();
                }
                else
                {
                    MessageBox.Show("Ничья!");
                    Close();
                }
        }

        private bool checkRules(int i, int j)
        {
            Tuple<int, int> last = pathIndex.Last();
            if (i >= fieldHeight || j >= fieldWidth || i<0 || j<0) return false;
            if (pathIndex.IndexOf(new Tuple<int, int>(i, j)) != -1) return false;
            if (penalty)
            {
                int sh1 = Math.Abs(last.Item1 - i);
                int sh2 = Math.Abs(last.Item2 - j);
                if ((sh1 == 0 || sh1 == 6) && (sh2 == 0 || sh2 == 6)) return true;
                else return false;
            }
            if (Math.Abs(last.Item1 - i) > 1 || Math.Abs(last.Item2 - j) > 1) return false;
            if (Math.Abs(last.Item1 -i) == 1 && Math.Abs(last.Item2 - j) == 1)
            {
                int i1 = pathIndex.IndexOf(new Tuple<int, int>(last.Item1 + (i > last.Item1 ? 1 : -1), last.Item2));
                int i2 = pathIndex.IndexOf(new Tuple<int, int>(last.Item1, last.Item2 + (j > last.Item2 ? 1 : -1)));
                if (i1 != -1 && i2 != -1 && Math.Abs(i1 - i2) == 1) return false;
            }
            return true;
        }

        private void pbField_SizeChanged(object sender, EventArgs e)
        {
            //TODO  minimize field size, when form minimized
            CalcPoints();
            PathRecal();
            Draw();
        }

        private void PathRecal()
        {
            path.Clear();
            foreach (Tuple<int, int> i in pathIndex) path.Add(new Point(
                 points[i.Item1, i.Item2].X + pointSize / 2,
                 points[i.Item1, i.Item2].Y + pointSize / 2));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show(label_player2.Text + " победил!");
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show(label_player1.Text + " победил!");
            Close();
        }
    }
}
