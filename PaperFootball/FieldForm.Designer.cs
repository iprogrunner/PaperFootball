﻿namespace PaperFootball
{
    partial class FieldForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbField = new System.Windows.Forms.PictureBox();
            this.label_player1 = new System.Windows.Forms.Label();
            this.label_player2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label_pl1_lines = new System.Windows.Forms.Label();
            this.label_pl2_lines = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pbField)).BeginInit();
            this.SuspendLayout();
            // 
            // pbField
            // 
            this.pbField.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbField.Location = new System.Drawing.Point(48, 50);
            this.pbField.Name = "pbField";
            this.pbField.Size = new System.Drawing.Size(678, 273);
            this.pbField.TabIndex = 0;
            this.pbField.TabStop = false;
            this.pbField.SizeChanged += new System.EventHandler(this.pbField_SizeChanged);
            this.pbField.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pbField_MouseClick);
            // 
            // label_player1
            // 
            this.label_player1.AutoSize = true;
            this.label_player1.Font = new System.Drawing.Font("Segoe Script", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_player1.Location = new System.Drawing.Point(12, 9);
            this.label_player1.Name = "label_player1";
            this.label_player1.Size = new System.Drawing.Size(115, 38);
            this.label_player1.TabIndex = 1;
            this.label_player1.Text = "Игрок1";
            // 
            // label_player2
            // 
            this.label_player2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label_player2.AutoSize = true;
            this.label_player2.Font = new System.Drawing.Font("Segoe Script", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_player2.Location = new System.Drawing.Point(647, 9);
            this.label_player2.Name = "label_player2";
            this.label_player2.Size = new System.Drawing.Size(115, 38);
            this.label_player2.TabIndex = 2;
            this.label_player2.Text = "Игрок2";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Font = new System.Drawing.Font("Segoe Script", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(174, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(426, 38);
            this.label1.TabIndex = 3;
            this.label1.Text = "vs";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_pl1_lines
            // 
            this.label_pl1_lines.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label_pl1_lines.AutoSize = true;
            this.label_pl1_lines.Font = new System.Drawing.Font("Segoe Script", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_pl1_lines.Location = new System.Drawing.Point(133, 9);
            this.label_pl1_lines.Name = "label_pl1_lines";
            this.label_pl1_lines.Size = new System.Drawing.Size(35, 38);
            this.label_pl1_lines.TabIndex = 4;
            this.label_pl1_lines.Text = "0";
            // 
            // label_pl2_lines
            // 
            this.label_pl2_lines.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_pl2_lines.AutoSize = true;
            this.label_pl2_lines.Font = new System.Drawing.Font("Segoe Script", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_pl2_lines.Location = new System.Drawing.Point(606, 9);
            this.label_pl2_lines.Name = "label_pl2_lines";
            this.label_pl2_lines.Size = new System.Drawing.Size(35, 38);
            this.label_pl2_lines.TabIndex = 5;
            this.label_pl2_lines.Text = "0";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.Enabled = false;
            this.button1.Location = new System.Drawing.Point(12, 50);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(30, 277);
            this.button1.TabIndex = 6;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Enabled = false;
            this.button2.Location = new System.Drawing.Point(732, 50);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(30, 277);
            this.button2.TabIndex = 7;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // FieldForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(774, 339);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label_pl2_lines);
            this.Controls.Add(this.label_pl1_lines);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label_player2);
            this.Controls.Add(this.label_player1);
            this.Controls.Add(this.pbField);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FieldForm";
            this.Text = "Игра";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.pbField)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbField;
        private System.Windows.Forms.Label label_player1;
        private System.Windows.Forms.Label label_player2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label_pl1_lines;
        private System.Windows.Forms.Label label_pl2_lines;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}

